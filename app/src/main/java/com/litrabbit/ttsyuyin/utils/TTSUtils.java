package com.litrabbit.ttsyuyin.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;

public class TTSUtils {
    private static final TTSUtils INSTANCE = new TTSUtils();
    private TextToSpeech textToSpeak;
    private boolean isSpeaking;

    private TTSUtils() {
    }

    /**
     * 播报
     *
     * @param context
     * @param text    文本内容
     */
    public static void speak(Context context, String text) {
        speak(context, text, 1.0f, 1.0f, false);
    }

    /**
     * 播报
     *
     * @param context
     * @param text             文本内容
     * @param ignoreIfSpeaking 如果在播报就忽略
     */
    public static void speak(Context context, String text, boolean ignoreIfSpeaking) {
        speak(context, text, 1.0f, 1.0f, ignoreIfSpeaking);
    }

    /**
     * 播报
     *
     * @param context
     * @param text             文本内容
     * @param rate             语速 1.0为正常
     * @param pitch            语调 1.0为正常
     * @param ignoreIfSpeaking 如果在播报就忽略
     */
    public static void speak(Context context, String text, float rate, float pitch, boolean ignoreIfSpeaking) {
        if (ignoreIfSpeaking && INSTANCE.isSpeaking) return;
        INSTANCE.speakNow(context, text, rate, pitch);
    }

    @SuppressLint("NewApi")
    private void speakNow(final Context context, final String text, final float rate, final float pitch) {
        if (textToSpeak == null) {
            textToSpeak = new TextToSpeech(context.getApplicationContext(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == 0) {
                        speakNow(context.getApplicationContext(), text, rate, pitch);
                    } else {
                        textToSpeak = null;
                    }
                }
            });
            textToSpeak.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                @Override
                public void onStart(String utteranceId) {
                    isSpeaking = true;
                }

                @Override
                public void onDone(String utteranceId) {
                    isSpeaking = false;
                }

                @Override
                public void onError(String utteranceId) {
                    isSpeaking = false;
                }
            });
        } else {
            textToSpeak.setSpeechRate(rate);
            textToSpeak.setPitch(pitch);
            textToSpeak.speak(text, TextToSpeech.QUEUE_FLUSH, null, "tts");
        }
    }
}