package com.litrabbit.ttsyuyin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.litrabbit.ttsyuyin.utils.TTSUtils;

public class MainActivity extends AppCompatActivity {

    /**
     * 记得要先安装TTS  apk引擎  不然无效 项目目录下面 kdxf_tts_3.0.apk 为引擎apk
     * @param savedInstanceState
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /**
         * TextView 文本
         */
        TextView textView = findViewById(R.id.text_view);
        /**
         * 语音合成
         */
        TTSUtils.speak(MainActivity.this,textView.getText().toString(),0.5f,0.5f,true);
    }
}